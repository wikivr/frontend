package config

import (
	"github.com/sirupsen/logrus"
	"os"
)

type Config struct {
	PublicDirPath string
	FrontendPort int
	ScraperService string
	ScraperPort int
	Logger logrus.Logger
}

func NewConfig(publicDirPath string, frontendPort int, scraperService string, scraperPort int) Config {
	logger := logrus.Logger{Out: os.Stderr, Level: logrus.InfoLevel}
	logger.SetFormatter(&logrus.JSONFormatter{})
	return Config{
		PublicDirPath: publicDirPath,
		FrontendPort: frontendPort,
		ScraperService: scraperService,
		ScraperPort: scraperPort,
		Logger: logger,
	}
}
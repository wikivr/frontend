package pkg

import (
	"fmt"
	"net/http"

	"bitbucket.org/wikivr/frontend/pkg/config"
	"bitbucket.org/wikivr/frontend/pkg/connections"
)

func Start(cfg config.Config) error {

	// Create a simple file server
	fs := http.FileServer(http.Dir(cfg.PublicDirPath))
	http.Handle("/", fs)

	conn, err := connections.Init(cfg)
	if err != nil {
		cfg.Logger.WithError(err).Error("unable to initialize connections")
		return err
	}

	// Configure websocket route
	http.HandleFunc("/ws", conn.HandleConnections)

	// Start listening for incoming chat messages
	go conn.HandleMessages()

	cfg.Logger.Infof("http server started on :%d", cfg.FrontendPort)
	err = http.ListenAndServe(fmt.Sprintf(":%d", cfg.FrontendPort), nil)
	if err != nil {
		cfg.Logger.WithError(err).Error("failed to ListenAndServer")
		return err
	}

	return nil
}


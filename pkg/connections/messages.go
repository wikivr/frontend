package connections

import (
	"context"
	"fmt"
	"strings"

	wikiqa "bitbucket.org/wikivr/frontend/pkg/proto/gen"
)

const (
	ActionAddUser = "add"
	ActionRemoveUser = "remove"
)

func (conn *Connections)HandleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-conn.broadcast
		conn.logger.Infof("Message received: %+v", msg)
		// Send it out to every client that is currently connected
		for client := range conn.clients {
			if err := client.WriteJSON(msg); err != nil {
				conn.logger.WithError(err).Error("unable to write message to client. Terminating recipient.")
				if err := client.Close(); err != nil {
					conn.logger.WithError(err).Error("unable to terminate recipient")
				}
				delete(conn.clients, client)
			}
		}
	}
}

func (conn *Connections)HandleActionMessages(msg Message) {
	var botMsg Message
	var actionPerformed bool
	var actionMsg string
	var handle string
	var gesture string

	splitMsg := strings.Split(strings.Title(msg.Message), " ")
	if len(splitMsg) < 1 {
		return
	}

	handle = strings.Join(splitMsg[1:], "_")
	botMsg = Message{
		Username: handle,
		Email: fmt.Sprintf("%s@wikivr.org", handle),
	}
	if strings.ToLower(splitMsg[0]) == ActionAddUser {
		actionPerformed = true
		actionMsg = "Adding"
		gesture = "Hello"
		conn.activeBot[handle] = true
		if err := conn.setHandle(handle); err != nil {
			return
		}
	} else if strings.ToLower(splitMsg[0]) == ActionRemoveUser {
		actionPerformed = true
		actionMsg = "Removing"
		gesture = "Bye"
		delete(conn.activeBot, handle)
	}

	if actionPerformed {
		conn.admin.Message = fmt.Sprintf("%s: %s", actionMsg, strings.ReplaceAll(handle, "_", " "))
		conn.broadcast <- conn.admin

		botMsg.Message = fmt.Sprintf("%s, %s!", gesture, msg.Username)
		conn.broadcast <- botMsg
		return
	}

	if len(conn.activeBot) == 0 {
		return
	}

	for handle, _ = range conn.activeBot {
	}
	conn.logger.Infof("question asked to %s", handle)
	req :=  &wikiqa.QuestionRequest{Handle:handle, Question:msg.Message}
	resp, err := conn.comprehensionClient.Ask(context.Background(), req)
	if err != nil {
		conn.logger.WithError(err).Errorf("unable to get response for Ask")
		return
	}


	botMsg.Username = handle
	botMsg.Email = fmt.Sprintf("%s@wikivr.org", handle)
	botMsg.Message = resp.Answer
	conn.broadcast <- botMsg
}

func (conn *Connections)setHandle(handle string) error {

	ctx := context.Background()

	docReq := &wikiqa.DocumentRequest{
		Handle: handle,
		Content: "",
	}

	resp, err := conn.comprehensionClient.SetDocument(ctx, docReq)
	if err != nil || resp.Code != wikiqa.UploadStatusCode_Ok {
		conn.logger.WithError(err).Error("failed to set document for %s: %+v", handle, resp)
	}

	return nil
}
package connections

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	"google.golang.org/grpc"

	"bitbucket.org/wikivr/frontend/pkg/config"
	wikiqa "bitbucket.org/wikivr/frontend/pkg/proto/gen"
)

func Init(cfg config.Config) (*Connections, error) {
	c := &Connections{
		clients: make(map[*websocket.Conn]bool),
		broadcast: make(chan Message),
		upgrader: websocket.Upgrader{},
		activeBot: map[string]bool{},
		admin: Message{
			Email: "admin@wikivr.org",
			Username: "Administrator",
		},
		logger: cfg.Logger,
	}

	err := c.setupScraperClient(fmt.Sprintf("%s:%d", cfg.ScraperService, cfg.ScraperPort))
	return c, err
}

func (conn *Connections)setupScraperClient(endpoint string) error {
	grpcClient, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		conn.logger.WithError(err).Error("failed to connect to server")
		return err
	}

	conn.comprehensionClient = wikiqa.NewCommunicationClient(grpcClient)

	return nil
}

func (conn *Connections) HandleConnections(w http.ResponseWriter, r *http.Request) {
	conn.logger.Infof("Handling new connection")
	// Upgrade initial GET request to a websocket
	ws, err := conn.upgrader.Upgrade(w, r, nil)
	if err != nil {
		conn.logger.WithError(err).Errorf("unable to upgrade request to a websocket")
		return
	}

	// make sure we close the connection when the function returns
	defer ws.Close()

	// register our new client
	conn.clients[ws] = true

	for {
		var msg Message
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			conn.logger.WithError(err).Error("failed to unmarshal JSON. Terminating client connection!")
			delete(conn.clients, ws)
			break
		}

		conn.broadcast <- msg

		conn.HandleActionMessages(msg)
	}
}

package connections

import (
	wikiqa "bitbucket.org/wikivr/frontend/pkg/proto/gen"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

// Define our message object
type Message struct {
	Email string `json:"email"`
	Username string `json:"username"`
	Message string `json:"message"`
}

type Connections struct {
	clients map[*websocket.Conn]bool
	broadcast chan Message
	upgrader websocket.Upgrader
	activeBot map[string]bool
	admin Message
	comprehensionClient wikiqa.CommunicationClient
	logger logrus.Logger
}

package main

import (
	"bitbucket.org/wikivr/frontend/pkg"
	"bitbucket.org/wikivr/frontend/pkg/config"
	"flag"
	"log"
)

var (
	publicDirPath = flag.String("public-dir-path", "/home/public/", "The files that will be publicly served via HTTP")
	scraperService = flag.String("scraper-service", "scraper-service", "The endpoint to connect to scraper service")
	scraperPort = flag.Int("scraper-port", 6592, "The port number of the scraper service")
	frontendPort = flag.Int("frontend-port", 8000, "The port number of the frontend")
)

func main()  {
	flag.Parse()

	cfg := config.NewConfig(*publicDirPath, *frontendPort, *scraperService, *scraperPort)

	if err := pkg.Start(cfg); err != nil {
		log.Fatalf("Failed to start the frontend. Error: %+v", err)
	}
}
#!/bin/bash

export GO111MODULE=on

# uncomment this to generate the protobuf go code
protoc --go_out=plugins=grpc:pkg/ proto/gen/comprehension.proto

# refresh vendor files and install frontend
go mod vendor
go install -v bitbucket.org/wikivr/frontend/cmd/frontend

module bitbucket.org/wikivr/frontend

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/websocket v1.4.0
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.22.0
)

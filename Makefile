# Registry for building images
REGISTRY ?= wikivr
VERSION ?= latest
FRONTEND_IMAGE ?= $(REGISTRY)/frontend

FRONTEND_TAG ?= "$(FRONTEND_IMAGE):$(VERSION)"

all: build

build: build_frontend

build_frontend:
	docker build --build-arg "VERSION=$(VERSION)" -t $(FRONTEND_TAG) -f ./build/Dockerfile .

run:
	docker run -it --rm --name wikivr_frontend -v "$$PWD:/go/src/bitbucket.org/wikivr/frontend" "$$(docker build -f build/Dockerfile.run --quiet .)"

build_cross_platform:
	env GOOS=linux GOARCH=amd64 GOARM=7 go build bitbucket.or/wikivr/frontend/cmd/frontend/

push:
	docker push $(FRONTEND_TAG)
